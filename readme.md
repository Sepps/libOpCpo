A customization point object library for the C++ operators. Providing the following features:

### Sane integer promotion rules

For example:
```
~u8(10) -> int
```
With this library:
```
bit_not(u8(10)) -> u8
```

The rules are:

The type of the result of infix operations on fundamental types is determined by:
1. If either side of the operator is a signed type, the result is a signed type.
2. The resulting type is the size of the larger of the two operands.
3. If either side of the operator is a floating point type, the result is a floating point type.

For example:
```
u8 + u8 -> u8
u8 + u16 -> u16
u8 + i16 -> i16
i8 + u16 -> i16
float + double -> double
```

### Provides a seperate remainder and modulus CPO
In the `<functional>` header, `std::modulus` exists. It is implemented as the remainder of a division with the operator `%`. This is not the `modulus`. For this reason, `OpCpo` provides seperate `CPO`'s.

### Allow perfect forwarding of user defined operators

## Provided CPO's
```CPP
inline constexpr Mod mod;
inline constexpr Rem rem;
inline constexpr Add add;
inline constexpr Sub sub;
inline constexpr Mul mul;
inline constexpr Div div;
inline constexpr LShift l_shift;
inline constexpr RShift r_shift;
inline constexpr BitXor bit_xor;
inline constexpr BitAnd bit_and;
inline constexpr BitOr bit_or;
inline constexpr BitXNor bit_xnor;
inline constexpr BoolXor bool_xor;
inline constexpr BoolXor bool_nequal;
inline constexpr BoolAnd bool_and;
inline constexpr BoolOr bool_or;
inline constexpr BoolXNor bool_xnor;
inline constexpr BoolXNor bool_equal;
inline constexpr Comma comma;
inline constexpr BitNot bit_not;
inline constexpr BoolNot bool_not;
inline constexpr UnaryPlus unary_plus;
inline constexpr UnaryMinus unary_minus;
inline constexpr UnaryDeref unary_deref;
inline constexpr PostInc post_inc;
inline constexpr PostDec post_dec;
inline constexpr PreInc pre_inc;
inline constexpr PreDec pre_dec;
```

# Using with ordinary operators

The library also provides a class `Fund<T>` which can be used with built in types. There are aliases and user defined literals for usage.