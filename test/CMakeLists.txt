cmake_minimum_required(VERSION 3.16.0)
project(OpCpo_Test VERSION 0.1.0 LANGUAGES CXX)

add_library(${PROJECT_NAME} OpCpo.test.cpp OpCpoFund.test.cpp)
target_link_libraries(${PROJECT_NAME} libOpCpo)
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 20)