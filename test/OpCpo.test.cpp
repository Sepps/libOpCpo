#include "cpos/OpCpo.hpp"
#include <cstdint>
#include <concepts>

using namespace OpCpo;

static_assert(std::is_same_v<int, Promote<int, unsigned>>);
static_assert(std::is_same_v<int long, Promote<int, unsigned long>>);
static_assert(std::is_same_v<int, Promote<unsigned, int>>);
static_assert(std::is_same_v<int long, Promote<unsigned int, long>>);
static_assert(std::is_same_v<double, Promote<float, double>>);
static_assert(std::is_same_v<double, Promote<double, float>>);
static_assert(std::is_same_v<double, Promote<double, uint64_t>>);
static_assert(std::is_same_v<double, Promote<uint64_t, double>>);
static_assert(std::is_same_v<double, Promote<double, uint8_t>>);
static_assert(std::is_same_v<double, Promote<uint8_t, double>>);
static_assert(std::is_same_v<float, Promote<float, uint8_t>>);
static_assert(std::is_same_v<float, Promote<uint8_t, float>>);
static_assert(std::is_same_v<double, Promote<float, uint64_t>>);
static_assert(std::is_same_v<double, Promote<uint64_t, float>>);

using u8 = uint8_t;
using u16 = uint16_t;
using i8 = int8_t;
using i16 = int16_t;

// mod
static_assert(mod(1,3)==1);
static_assert(mod(-1,3)==2);
static_assert(mod(2,2)==0);
static_assert(mod(2,-2)==0);
static_assert(mod(-1,-3)==-1);
static_assert(requires{ { mod(i8(1), u16(1)) } -> std::same_as<i16>; });
// rem
static_assert(rem(5, 7) == 5);
static_assert(rem(0, 7) == 0);
static_assert(rem(-2, 7) == -2);
static_assert(requires{ { rem(i8(1), u16(1)) } -> std::same_as<i16>; });
// add
static_assert(add(1, 2) == 3);
static_assert(requires{ { add(i8(1), u16(1)) } -> std::same_as<i16>; });
// sub
static_assert(sub(3, 2) == 1);
static_assert(requires{ { sub(i8(1), u16(1)) } -> std::same_as<i16>; });
// mul
static_assert(mul(2, 3) == 6);
static_assert(requires{ { mul(i8(1), u16(1)) } -> std::same_as<i16>; });
// div
static_assert(OpCpo::div(6, 3) == 2);
static_assert(requires{ { OpCpo::div(i8(1), u16(1)) } -> std::same_as<i16>; });
// l_shift
static_assert(l_shift(1, 2u) == 4);
static_assert(requires{ { r_shift(i8(1), u16(1)) } -> std::same_as<i8>; });
// bit_xor
static_assert(bit_xor(1, 1) == 0);
static_assert(bit_xor(0, 0) == 0);
static_assert(bit_xor(1, 2) == 3);
static_assert(requires{ { bit_xor(i8(1), u16(1)) } -> std::same_as<i16>; });
// bit_and
static_assert(bit_and(1, 2) == 0);
static_assert(bit_and(1, 1) == 1);
static_assert(requires{ { bit_and(i8(1), u16(1)) } -> std::same_as<i16>; });
// bit_or
static_assert(bit_or(1, 2) == 3);
static_assert(bit_or(1, 1) == 1);
static_assert(requires{ { bit_or(i8(1), u16(1)) } -> std::same_as<i16>; });
// bit_xnor
static_assert(bit_xnor(uint8_t(0), uint8_t(0)) == uint8_t(0xff));
static_assert(bit_xnor(uint8_t(1), uint8_t(1)) == uint8_t(0xff));
static_assert(bit_xnor(uint8_t(0), uint8_t(1)) == uint8_t(0b11111110));
static_assert(requires{ { bit_xnor(i8(1), u16(1)) } -> std::same_as<i16>; });
// bool_xor, bool_nequal
static_assert(bool_xor(true, false) == true);
static_assert(bool_xor(false, true) == true);
static_assert(bool_xor(true, true) == false);
static_assert(bool_xor(false, false) == false);
static_assert(requires{ { bool_xor(i8(1), u16(1)) } -> std::same_as<bool>; });
// bool_and
static_assert(bool_and(true, true) == true);
static_assert(bool_and(false, true) == false);
static_assert(bool_and(true, false) == false);
static_assert(bool_and(false, false) == false);
static_assert(requires{ { bool_and(i8(1), u16(1)) } -> std::same_as<bool>; });
// bool_or
static_assert(bool_or(true, true) == true);
static_assert(bool_or(false, true) == true);
static_assert(bool_or(true, false) == true);
static_assert(bool_or(false, false) == false);
static_assert(requires{ { bool_or(i8(1), u16(1)) } -> std::same_as<bool>; });
// bool_xnor, bool_equal
static_assert(bool_xnor(true, true) == true);
static_assert(bool_xnor(false, true) == false);
static_assert(bool_xnor(true, false) == false);
static_assert(bool_xnor(false, false) == true);
static_assert(requires{ { bool_xnor(i8(1), u16(1)) } -> std::same_as<bool>; });
// comma
static_assert(comma(1, 2) == 2);
static_assert(requires{ { comma(i8(1), u16(1)) } -> std::convertible_to<u16>; });
static_assert(requires{ { comma(i16(1), i8(1)) } -> std::convertible_to<i8>; });
// bit_not
static_assert(bit_not(uint8_t(2)) == uint8_t(~uint8_t(2)));
static_assert(requires{ { bit_not(i8(1)) } -> std::same_as<i8>; });
// bool_not
static_assert(bool_not(1) == false);
static_assert(bool_not(false) == true);
static_assert(requires{ { bool_not(i8(1)) } -> std::same_as<bool>; });
// unary_plus
static_assert(unary_plus(1) == 1);
static_assert(requires{ { unary_plus(i8(1)) } -> std::same_as<i8>; });
// unary_minus
static_assert(unary_minus(1) == -1);
static_assert(requires{ { unary_minus(i8(1)) } -> std::same_as<i8>; });
// unary_deref
constexpr int value = 10;
static_assert(unary_deref(&value) == 10);
static_assert(requires{ { unary_deref(&value) } -> std::same_as<const int&>; });
// post_inc
static constexpr int post_inc_test()
{
	int v = 0;
	(void)post_inc(v);
	(void)post_inc(v);
	return v;
}
static_assert(post_inc_test() == 2);
static_assert(requires{ { post_inc(std::declval<int&>()) } -> std::same_as<int>; });
// post_dec
static constexpr int post_dec_test()
{
	int v = 2;
	(void)post_dec(v);
	(void)post_dec(v);
	return v;
}
static_assert(post_dec_test() == 0);
static_assert(requires{ { post_dec(std::declval<int&>()) } -> std::same_as<int>; });
// pre_inc
static constexpr int pre_inc_test()
{
	int v = 0;
	(void)pre_inc(v);
	return pre_inc(v);
}
static_assert(pre_inc_test() == 2);
static_assert(requires{ { pre_inc(pre_inc(std::declval<int&>())) } -> std::same_as<int&>; });
// pre_dec
static constexpr int pre_dec_test()
{
	int v = 2;
	(void)pre_dec(v);
	return pre_dec(v);
}
static_assert(pre_dec_test() == 0);
static_assert(requires{ { pre_dec(pre_dec(std::declval<int&>())) } -> std::same_as<int&>; });