#include "cpos/OpCpoFund.hpp"
#include <cstdint>
#include <concepts>

using namespace OpCpo;
using namespace OpCpo::Udl;

// rem
static_assert(rem(5_oi32, 7_oi32) == 5_oi32);
static_assert(rem(0_oi32, 7_oi32) == 0_oi32);
static_assert(rem(-2_oi32, 7_oi32) == -2_oi32);
static_assert(requires{ { rem(1_oi8, 1_oi16) } -> std::same_as<oi16>; });
// add
static_assert(add(1, 2) == 3);
static_assert(requires{ { add((1_oi8), (1_ou16)) } -> std::same_as<oi16>; });
// sub
static_assert(sub(3, 2) == 1);
static_assert(requires{ { sub((1_oi8), (1_ou16)) } -> std::same_as<oi16>; });
// mul
static_assert(mul(2, 3) == 6);
static_assert(requires{ { mul((1_oi32), (1_oi32)) } -> std::same_as<oi32>; });
// div
static_assert(OpCpo::div(6_oi32, 3_oi32) == 2_oi32);
static_assert(requires{ { OpCpo::div((1_oi8), (1_oi16)) } -> std::same_as<oi16>; });
// l_shift
static_assert(l_shift(1_oi32, 2_ou32) == 4_oi32);
static_assert(requires{ { r_shift((1_oi8), (1_ou32)) } -> std::same_as<oi8>; });
// bit_xor
static_assert(bit_xor(1_oi32, 1_oi32) == 0_oi32);
static_assert(bit_xor(0_oi32, 0_oi32) == 0_oi32);
static_assert(bit_xor(1_oi32, 2_oi32) == 3_oi32);
static_assert(requires{ { bit_xor((1_oi8), (1_oi16)) } -> std::same_as<oi16>; });
// bit_and
static_assert(bit_and(1_oi32, 2_oi32) == 0_oi32);
static_assert(bit_and(1_oi32, 1_oi32) == 1_oi32);
static_assert(requires{ { bit_and((1_oi16), (1_oi8)) } -> std::same_as<oi16>; });
// bit_or
static_assert(bit_or(1_oi32, 2_oi32) == 3_oi32);
static_assert(bit_or(1_oi32, 1_oi32) == 1_oi32);
static_assert(requires{ { bit_or((1_oi8), (1_oi16)) } -> std::same_as<oi16>; });
// bit_xnor
static_assert(bit_xnor((0_oi8), (0_oi8)) == (0xff_oi8));
static_assert(bit_xnor((1_oi8), (1_oi8)) == (0xff_oi8));
static_assert(bit_xnor((0_oi8), (1_oi8)) == (0b11111110_oi8));
static_assert(requires{ { bit_xnor((1_oi8), (1_oi16)) } -> std::same_as<oi16>; });
// bool_xor, bool_nequal
static_assert(bool_xor(true, false) == true);
static_assert(bool_xor(false, true) == true);
static_assert(bool_xor(true, true) == false);
static_assert(bool_xor(false, false) == false);
static_assert(requires{ { bool_xor((1_oi32), (1_oi32)) } -> std::same_as<bool>; });
// bool_and
static_assert(bool_and(true, true) == true);
static_assert(bool_and(false, true) == false);
static_assert(bool_and(true, false) == false);
static_assert(bool_and(false, false) == false);
static_assert(requires{ { bool_and((1_oi32), (1_oi32)) } -> std::same_as<bool>; });
// bool_or
static_assert(bool_or(true, true) == true);
static_assert(bool_or(false, true) == true);
static_assert(bool_or(true, false) == true);
static_assert(bool_or(false, false) == false);
static_assert(requires{ { bool_or((1_oi32), (1_oi32)) } -> std::same_as<bool>; });
// bool_xnor, bool_equal
static_assert(bool_xnor(true, true) == true);
static_assert(bool_xnor(false, true) == false);
static_assert(bool_xnor(true, false) == false);
static_assert(bool_xnor(false, false) == true);
static_assert(requires{ { bool_xnor((1_oi32), (1_oi32)) } -> std::same_as<bool>; });
// comma
static_assert(comma(1_oi32, 2_oi32) == 2_oi32);
static_assert(requires{ { comma(1_oi8, 1_oi32) } -> std::convertible_to<oi32>; });
static_assert(requires{ { comma(1_oi8, 1_oi32) } -> std::convertible_to<oi32>; });
// bit_not
static_assert(bit_not(2_oi32) == ~2_oi32);
static_assert(requires{ { bit_not((1_oi32)) } -> std::same_as<oi32>; });
// bool_not
static_assert(bool_not(1_oi32) == false);
static_assert(bool_not(false) == true);
static_assert(requires{ { bool_not((1_oi32)) } -> std::same_as<bool>; });
// unary_plus
static_assert(unary_plus(1_oi32) == 1_oi32);
static_assert(requires{ { unary_plus((1_oi32)) } -> std::same_as<oi32>; });
// unary_minus
static_assert(unary_minus(1_oi32) == -1_oi32);
static_assert(requires{ { unary_minus((1_oi32)) } -> std::same_as<oi32>; });
// unary_deref
constexpr auto value = 10_oi32;
static_assert(unary_deref(&value) == 10_oi32);
static_assert(requires{ { unary_deref(&value) } -> std::same_as<const oi32&>; });
// post_inc
static_assert(post_inc(post_inc(0_oi32)) == 0_oi32);
static constexpr auto post_inc_test()
{
	auto v = 0_oi32;
	(void)post_inc(v);
	(void)post_inc(v);
	return v;
}
static_assert(post_inc_test() == 2_oi32);
static_assert(requires{ { post_inc(0_oi32) } -> std::same_as<oi32>; });
// post_dec
static_assert(post_dec(post_dec(2_oi32)) == 2_oi32);
static constexpr auto post_dec_test()
{
	auto v = 2_oi32;
	(void)post_dec(v);
	(void)post_dec(v);
	return v;
}
static_assert(post_dec_test() == 0_oi32);
static_assert(requires{ { post_dec(0_oi32) } -> std::same_as<oi32>; });
// pre_inc
static constexpr oi32 pre_inc_test()
{
	auto v = 0_oi32;
	(void)pre_inc(v);
	return pre_inc(v);
}
static_assert(pre_inc_test() == 2_oi32);
static_assert(requires{ { pre_inc(pre_inc(std::declval<oi32&>())) } -> std::same_as<oi32&>; });
// pre_dec
static constexpr oi32 pre_dec_test()
{
	auto v = 2_oi32;
	(void)pre_dec(v);
	return pre_dec(v);
}
static_assert(pre_dec_test() == 0_oi32);
static_assert(requires{ { pre_dec(pre_dec(std::declval<oi32&>())) } -> std::same_as<oi32&>; });