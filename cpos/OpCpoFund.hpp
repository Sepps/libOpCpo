#pragma once
#include "OpCpo.hpp"

namespace OpCpo
{
	template <auto Tag>
	struct FundTag{};
	template <Fundamental T, auto Tag = 0>
	struct Fund
	{
		T value{};
		[[no_unique_address]] FundTag<Tag> tag;

		[[nodiscard]] constexpr Fund operator+() const noexcept
		{
			return { unary_plus(value) };
		}
		[[nodiscard]] constexpr Fund operator-() const noexcept
		{
			return { unary_minus(value) };
		}

		[[nodiscard]] constexpr Fund operator~() const noexcept
		{
			return { bit_not(value) };
		}
		[[nodiscard]] constexpr Fund operator++(int) noexcept
		{
			return { post_inc(value) };
		}
		[[nodiscard]] constexpr Fund operator--(int) noexcept
		{
			return { post_dec(value) };
		}
		[[nodiscard]] constexpr bool operator!() const noexcept
		{
			return bool_not(value);
		}

		constexpr Fund& operator++() noexcept
		{
			pre_inc(value);
			return *this;
		}
		constexpr Fund& operator--() noexcept
		{
			pre_dec(value);
			return *this;
		}
		constexpr Fund& operator =(const Fund& input) noexcept
		{
			value = input.value;
			return *this;
		}
		constexpr Fund& operator +=(const Fund& input) noexcept
		{
			return (*this = *this + input);
		}
		constexpr Fund& operator -=(const Fund& input) noexcept
		{
			return (*this = *this - input);
		}
		constexpr Fund& operator *=(const Fund& input) noexcept
		{
			return (*this = *this * input);
		}
		constexpr Fund& operator /=(const Fund& input) noexcept
		{
			return (*this = *this / input);
		}
		constexpr Fund& operator %=(const Fund& input) noexcept
		{
			return (*this = *this % input);
		}
		constexpr Fund& operator &=(const Fund& input) noexcept
		{
			return (*this = *this & input);
		}
		constexpr Fund& operator |=(const Fund& input) noexcept
		{
			return (*this = *this | input);
		}
		constexpr Fund& operator ^=(const Fund& input) noexcept
		{
			return (*this = *this ^ input);
		}
		template <typename Rhs>
		constexpr Fund& operator <<=(const Rhs& input) noexcept requires(std::is_unsigned_v<Rhs>)
		{
			return (*this = *this << input);
		}
		template <typename Rhs>
		constexpr Fund& operator >>=(const Rhs& input) noexcept requires(std::is_unsigned_v<Rhs>)
		{
			return (*this = *this >> input);
		}
	};

	template <Fundamental T>
	Fund(T)->Fund<T>;
	template <Fundamental T, auto Tag>
	Fund(T, FundTag<Tag>)->Fund<T, Tag>;

	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator+(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ add(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator-(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ sub(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator*(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ mul(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator/(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ div(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator%(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ rem(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator&(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ bit_and(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator|(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ bit_or(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr auto operator^(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ bit_xor(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
		requires (std::is_unsigned_v<Rhs>)
	[[nodiscard]] constexpr auto operator<<(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ l_shift(lhs.value, rhs.value), FundTag<Tag>{} };
	}
	template <typename Lhs, typename Rhs, auto Tag>
		requires (std::is_unsigned_v<Rhs>)
	[[nodiscard]] constexpr auto operator>>(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return Fund{ r_shift(lhs.value, rhs.value), FundTag<Tag>{} };
	}

	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr bool operator&&(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return bool_and(lhs.value, rhs.value);
	}
	template <typename Lhs, typename Rhs, auto Tag>
	[[nodiscard]] constexpr bool operator||(Fund<Lhs, Tag> const &lhs, Fund<Rhs, Tag> const &rhs) noexcept
	{
		return bool_or(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator ==(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return equal(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator !=(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return nequal(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator <(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return less(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator >(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return greater(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator <=(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return less_or_equal(lhs.value, rhs.value);
	}
	template <typename Lhs, auto Tag>
	[[nodiscard]] constexpr bool operator >=(Fund<Lhs, Tag> const &lhs, Fund<Lhs, Tag> const &rhs) noexcept
	{
		return greater_or_equal(lhs.value, rhs.value);
	}

	using ou8 = Fund<uint8_t>;
	using ou16 = Fund<uint16_t>;
	using ou32 = Fund<uint32_t>;
	using oi8 = Fund<int8_t>;
	using oi16 = Fund<int16_t>;
	using oi32 = Fund<int32_t>;
	using oi64 = Fund<int64_t>;
	using osz = Fund<size_t>;
	using ossz = Fund<std::make_signed_t<size_t>>;
	using oiptr = Fund<intptr_t>; 
	using ouptr = Fund<uintptr_t>; 
	using odptr = Fund<ptrdiff_t>; 

	namespace Udl
	{
		[[nodiscard]] constexpr ou8 operator""_ou8(unsigned long long value) noexcept
		{
			return { static_cast<uint8_t>(value) };
		}
		[[nodiscard]] constexpr ou16 operator""_ou16(unsigned long long value) noexcept
		{
			return { static_cast<uint16_t>(value) };
		}
		[[nodiscard]] constexpr ou32 operator""_ou32(unsigned long long value) noexcept
		{
			return { static_cast<uint32_t>(value) };
		}
		using ou64 = Fund<uint64_t>;
		[[nodiscard]] constexpr ou64 operator""_ou64(unsigned long long value) noexcept
		{
			return { static_cast<uint64_t>(value) };
		}
		[[nodiscard]] constexpr oi8 operator""_oi8(unsigned long long value) noexcept
		{
			return { static_cast<int8_t>(value) };
		}
		[[nodiscard]] constexpr oi16 operator""_oi16(unsigned long long value) noexcept
		{
			return { static_cast<int16_t>(value) };
		}
		[[nodiscard]] constexpr oi32 operator""_oi32(unsigned long long value) noexcept
		{
			return { static_cast<int32_t>(value) };
		}
		[[nodiscard]] constexpr oi64 operator""_oi64(unsigned long long value) noexcept
		{
			return { static_cast<int64_t>(value) };
		}
		[[nodiscard]] constexpr osz operator""_osz(unsigned long long value) noexcept
		{
			return { static_cast<size_t>(value) };
		}
		[[nodiscard]] constexpr ossz operator""_ossz(unsigned long long value) noexcept
		{
			return { static_cast<std::make_signed_t<size_t>>(value) };
		}
		[[nodiscard]] constexpr oiptr operator""_ointptr(unsigned long long value) noexcept
		{
			return { static_cast<intptr_t>(value) };
		}
		[[nodiscard]] constexpr ouptr operator""_ouintptr(unsigned long long value) noexcept
		{
			return { static_cast<uintptr_t>(value) };
		}
		[[nodiscard]] constexpr odptr operator""_optrdiff(unsigned long long value) noexcept
		{
			return { static_cast<ptrdiff_t>(value) };
		}
	}
}