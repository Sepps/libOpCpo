#pragma once
#include <type_traits>

namespace OpCpo
{
	template<typename T>
	constexpr decltype( sizeof( char ) ) bits_in() noexcept
	{
		using u8 = unsigned char;
		auto out = sizeof( T );
		auto byte = static_cast<u8>( ~u8( 0 ) );
		decltype( out ) i = 0;
		for ( ; byte != 0; ++i ) { byte >>= 1; }
		return sizeof( T ) * i;
	}

	template<typename T>
	concept Fundamental = std::is_fundamental_v<std::remove_cvref_t<T>>;
	template<typename T>
	concept NFundamental = not std::is_fundamental_v<std::remove_cvref_t<T>>;
}