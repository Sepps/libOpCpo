#pragma once
#include "OpCpoCommon.hpp"
#include <bit>
#include <cstddef>
#include <cstdint>
#include <type_traits>

#define fwd( x ) static_cast<decltype( x ) &&>( x )
namespace OpCpo
{
	template<typename Lhs, typename Rhs>
	using GetLargerT = std::conditional_t<( sizeof( Lhs ) > sizeof( Rhs ) ), Lhs, Rhs>;

	template<size_t Size>
	using GetFloatOfSize = std::conditional_t<Size == sizeof( double ), double, float>;

	template<typename Lhs, typename Rhs, typename Larger = GetLargerT<Lhs, Rhs>>
	using InfectWithFloat = std::conditional_t<(std::is_floating_point_v<Lhs> or std::is_floating_point_v<Rhs>), GetFloatOfSize<sizeof( Larger )>, Larger>;

	template<typename T>
	struct AddSignImpl
	{
		using type = T;
	};
	template<typename T>
	requires( std::is_integral_v<T> ) struct AddSignImpl<T>
	{
		using type = std::make_signed_t<T>;
	};
	template<typename T>
	using AddSign = typename AddSignImpl<T>::type;

	template<typename Lhs, typename Rhs, typename L = InfectWithFloat<Lhs, Rhs>>
	using Promote = std::conditional_t<(std::is_signed_v<Lhs> or std::is_signed_v<Rhs>), AddSign<L>, L>;

	struct Mod
	{
		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs n, Rhs d ) const noexcept -> Promote<Lhs, Rhs>
		{
			return impl<Promote<Lhs, Rhs>>( n, d );
		}
	private:
		template<typename T>
		static constexpr auto impl( T n, T d ) -> T
		{
			auto offset = std::make_unsigned_t<T>( d ) >> ( bits_in<T>() - 1 );
			// sarah from #include
			return mod_pos<T>( n - offset, d ) + offset;
		}
		template<typename T>
		static constexpr auto mod_pos( T n, T d ) -> T
		{
			auto r = n % d;
			auto mask = ( n ^ d ) >> ( bits_in<T>() - 1 );
			return r + ( mask & d );
		}
	};

	struct Rem
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && value, Rhs && mod ) const noexcept
		{
			return fwd( value ) % mod;
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( value % mod );
		}
	};

	struct Add
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) + fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Promote<Lhs, Rhs>
		{
			if constexpr (std::is_integral_v<Lhs> and std::is_integral_v<Rhs>)
			{
				return static_cast<Promote<Lhs, Rhs>>
				(
					static_cast<std::make_unsigned_t<Lhs>>(lhs) +
					static_cast<std::make_unsigned_t<Rhs>>(rhs)
				);
			}
			else
			{
				return fwd( lhs ) + fwd( rhs );
			}
		}
	};

	struct Sub
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) - fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Promote<Lhs, Rhs>
		{
			if constexpr (std::is_integral_v<Lhs> and std::is_integral_v<Rhs>)
			{
				return static_cast<Promote<Lhs, Rhs>>
				(
					static_cast<std::make_unsigned_t<Lhs>>(lhs) -
					static_cast<std::make_unsigned_t<Rhs>>(rhs)
				);
			}
			else
			{
				return fwd( lhs ) - fwd( rhs );
			}
		}
	};

	struct Mul
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) * fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Promote<Lhs, Rhs>
		{	
			if constexpr (std::is_integral_v<Lhs> and std::is_integral_v<Rhs>)
			{
				return static_cast<Promote<Lhs, Rhs>>
				(
					static_cast<std::make_unsigned_t<Lhs>>(lhs) *
					static_cast<std::make_unsigned_t<Rhs>>(rhs)
				);
			}
			else
			{
				return fwd( lhs ) * fwd( rhs );
			}
		}
	};

	struct Div
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) / fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( lhs / rhs );
		}
	};

	struct LShift
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) << fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs> requires(std::is_unsigned_v<Rhs>)
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Lhs
		{
			return static_cast<Promote<Lhs, Rhs>>( lhs << rhs );
		}
	};

	struct RShift
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) >> fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs> requires(std::is_unsigned_v<Rhs>)
		[[nodiscard]] constexpr auto operator()( Lhs lhs, Rhs rhs ) const noexcept -> Lhs
		{
			return static_cast<Promote<Lhs, Rhs>>( lhs >> rhs );
		}
	};

	struct BitXor
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) ^ fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( value ^ mod );
		}
	};

	struct BitAnd
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) & fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( value & mod );
		}
	};

	struct BitOr
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) | fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( value | mod );
		}
	};

	struct BitXNor
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ~( fwd( lhs ) ^ fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> Promote<Lhs, Rhs>
		{
			return static_cast<Promote<Lhs, Rhs>>( ~( value ^ mod ) );
		}
	};

	struct BoolXor
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) != fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value != mod;
		}
	};

	struct BoolAnd
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) && fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return !!( value && mod );
		}
	};

	struct BoolOr
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return fwd( lhs ) || fwd( rhs );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return !!( value || mod );
		}
	};

	struct BoolXNor
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ) == fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value == mod;
		}
	};

	struct BoolLess
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ) < fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value < mod;
		}
	};

	struct BoolGreater
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ) > fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value > mod;
		}
	};

	struct BoolLessOrEqual
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ) <= fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value <= mod;
		}
	};

	struct BoolGreaterOrEqual
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ) >= fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs value, Rhs mod ) const noexcept -> bool
		{
			return value >= mod;
		}
	};

	struct Comma
	{
		template <typename Lhs, typename Rhs> requires ((NFundamental<Lhs> or NFundamental<Rhs>))
		[[nodiscard]] constexpr decltype( auto ) operator()( Lhs && lhs, Rhs && rhs ) const noexcept
		{
			return ( fwd( lhs ), fwd( rhs ) );
		}

		template<Fundamental Lhs, Fundamental Rhs>
		[[nodiscard]] constexpr auto operator()( Lhs && value, Rhs && mod ) const noexcept -> Rhs &&
		{
			return ( static_cast<void>(value), fwd(mod) );
		}
	};

	struct BitNot
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return ~fwd( value );
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return static_cast<T>( ~value );
		}
	};

	struct BoolNot
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return !fwd( value );
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> bool
		{
			return !value;
		}
	};

	struct UnaryPlus
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return +fwd( value );
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return +value;
		}
	};

	struct PopCount
	{
		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return static_cast<T>( std::popcount( fwd( value ) ) );
		}
	};

	struct CountLeadingZeros
	{
		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return static_cast<T>( std::countl_zero( fwd( value ) ) );
		}
	};

	struct CountTrailingZeros
	{
		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return static_cast<T>( std::countr_zero( fwd( value ) ) );
		}
	};

	struct UnaryMinus
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return -fwd( value );
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T value ) const noexcept -> T
		{
			return -value;
		}
	};

	struct UnaryDeref
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return *fwd( value );
		}

		template<typename T>
		[[nodiscard]] constexpr auto operator()( T * value ) const noexcept -> T &
		{
			return *value;
		}
	};

	struct PostInc
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return fwd( value )++;
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T & value ) const noexcept -> std::remove_cvref_t<T>
		{
			using ty = std::make_unsigned_t<std::remove_cvref_t<T>>;
			auto out = value;
			return
			(
				( value = static_cast<std::remove_cvref_t<T>>(static_cast<ty>(value) + ty(1)) ),
				out
			);
		}
	};

	struct PostDec
	{
		template<NFundamental T>
		[[nodiscard]] constexpr decltype( auto ) operator()( T && value ) const noexcept
		{
			return fwd( value )--;
		}

		template<Fundamental T>
		[[nodiscard]] constexpr auto operator()( T & value ) const noexcept -> std::remove_cvref_t<T>
		{
			using ty = std::make_unsigned_t<std::remove_cvref_t<T>>;
			auto out = value;
			return
			(
				( value = static_cast<std::remove_cvref_t<T>>(static_cast<ty>(out) - ty(1))),
				out
			);
		}
	};

	struct PreInc
	{
		template<NFundamental T>
		constexpr decltype( auto ) operator()( T & value ) const noexcept
		{
			return ++value;
		}

		template<Fundamental T>
		constexpr auto operator()( T & value ) const noexcept -> T &
		{
			using ty = std::make_unsigned_t<std::remove_cvref_t<T>>;
			return (value = static_cast<std::remove_cvref_t<T>>(static_cast<ty>(value) + ty(1)));
		}
	};

	struct PreDec
	{
		template<NFundamental T>
		constexpr decltype( auto ) operator()( T & value ) const noexcept
		{
			return --value;
		}

		template<Fundamental T>
		constexpr auto operator()( T & value ) const noexcept -> T &
		{
			using ty = std::make_unsigned_t<std::remove_cvref_t<T>>;
			return (value = static_cast<std::remove_cvref_t<T>>(static_cast<ty>(value) - ty(1)));
		}
	};

	inline namespace CPO
	{
		inline constexpr CountLeadingZeros countl_zero;
		inline constexpr CountTrailingZeros countr_zero;
		inline constexpr PopCount pop_count;
		inline constexpr Mod mod;
		inline constexpr Rem rem;
		inline constexpr Add add;
		inline constexpr Sub sub;
		inline constexpr Mul mul;
		inline constexpr Div div;
		inline constexpr LShift l_shift;
		inline constexpr RShift r_shift;
		inline constexpr BitXor bit_xor;
		inline constexpr BitAnd bit_and;
		inline constexpr BitOr bit_or;
		inline constexpr BitXNor bit_xnor;
		inline constexpr BoolXor bool_xor;
		inline constexpr BoolXor bool_nequal;
		inline constexpr BoolAnd bool_and;
		inline constexpr BoolOr bool_or;
		inline constexpr BoolXNor bool_xnor;
		inline constexpr BoolXNor bool_equal;
		inline constexpr BoolXor nequal;
		inline constexpr BoolXNor equal;
		inline constexpr BoolLess less;
		inline constexpr BoolGreater greater;
		inline constexpr BoolLessOrEqual less_or_equal;
		inline constexpr BoolGreaterOrEqual greater_or_equal;
		inline constexpr Comma comma;
		inline constexpr BitNot bit_not;
		inline constexpr BoolNot bool_not;
		inline constexpr UnaryPlus unary_plus;
		inline constexpr UnaryMinus unary_minus;
		inline constexpr UnaryDeref unary_deref;
		inline constexpr PostInc post_inc;
		inline constexpr PostDec post_dec;
		inline constexpr PreInc pre_inc;
		inline constexpr PreDec pre_dec;
	}
}
#undef fwd
